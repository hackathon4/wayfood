import React from 'react';
import { Category } from './category';
import './category-screen.css'

export function CategoriesScreen({ categories }) {
	return (
		<div className="screen categories-screen">
			<div className="categories-pane">
				{ categories.map((category) => <Category key={`category_${category.id}`} { ...category }/>)}
			</div>
		</div>
	);
}