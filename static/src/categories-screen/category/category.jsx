import React from 'react';
import { Button } from '../../components/button';
import { routeToMenu } from '../../router/actions';
import { RouterContext } from '../../router';
import './category.css';

export function Category({name, imgSrc, id}) {
	const routeDispatch = React.useContext(RouterContext);
	return (
		<div className="categories__category" onClick={ () => routeDispatch(routeToMenu(id)) }>
			<img src={ imgSrc } />
			<Button className="category__button">{ name }</Button>
		</div>
	);
}