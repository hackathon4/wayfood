import React from 'react';

export const RouterContext = React.createContext(() => {});

export const Route = {
	Main: "main",
	Categories: "categories-screen",
	Menu: "menu-screen",
	Product: "product-view",
	Order: "Order-view",
}

export const initRouteState = {
	basket: false,
	backButton: false,
	route: Route.Main,
	categoryId: null,
	product: null,
};

export function routerReducer(state, action) {
	console.log(action.type)
	switch (action.type) {
		case Route.Main:
			return initRouteState
		case Route.Categories:
			return { route: Route.Categories, basket: true, backButton: false,  categoryId: null, product: null }
		case Route.Menu:
			return { route: Route.Menu, basket: true, backButton: true, categoryId: action.categoryId, product: null }
		case Route.Product:
			return { route: Route.Product, basket: true, backButton: true, categoryId: state.categoryId, product: action.product }
		case Route.Order:
			return { route: Route.Order, basket: false, backButton: true, categoryId: null, product: null }
		case 'return-back':
			return returnBack(state)
		default:
			return initRouteState;
	}
}

function returnBack(state) {
	switch (state.route) {
		case Route.Product:
			return { route: Route.Menu, basket: true, backButton: true, categoryId: state.categoryId, product: null }
		case Route.Menu:
			return { route: Route.Categories, basket: true, backButton: false, categoryId:null, product: null }
		case Route.Order:
			return { route: Route.Categories, basket: true, backButton: false, categoryId:null, product: null }
		default:
			return state;
	}
}