import { Route } from './router';

export const routeToMain = { type: Route.Main };
export const routeToCategories = { type: Route.Categories };
export const routeToMenu = (categoryId) => ({ type: Route.Menu, categoryId });
export const routeToProduct = (product) => ({ type: Route.Product, product });
export const routeToOrder = { type: Route.Order };
export const returnBack = { type: 'return-back' };
