
export const addProduct = (product, count = 1) => ({ type: 'add', product, count });
export const removeProduct = (product) => ({ type: 'remove', product });
export const resetProduct = () => ({ type: 'reset' });