import React from 'react';
import { priceFormatter } from '../utils/price';
import { RouterContext } from '../router';
import { routeToOrder } from '../router/actions';
import './basket.css';

export const BasketContext = React.createContext(() => {});

export function Basket({ totalCost, products }) {
	const routeDispatch = React.useContext(RouterContext);
	return (
		<div className="basket-pose" onClick={ () => routeDispatch(routeToOrder) }>
			<h5>заказ</h5>
			<div>
				{ priceFormatter(totalCost) } р
			</div>
		</div>
	);
}

export const defaultBasket = {
	products: {},
	productsCount: {},
	totalCost: 0,
}

function calculateTotalCost(products, productsCount) {
	let total = 0;
	for(const product in products) {
		const { id, cost } = products[product]
		const count = productsCount[id];
		total += count * cost;
	}
	return total;
}


export function routerBasket(state, action) {
	switch (action.type) {
		case 'add': {
			const { product, count } = action;
			const { id } = product;
			if (id in state.productsCount) {
				const { products } = state;
				const productsCount = { ...state.productsCount, [id]: state.productsCount[id] + count}
				const totalCost = calculateTotalCost(products, productsCount)
				return { productsCount, products, totalCost };
			} else {
				const products = { ...state.products, [id]: product };
				const productsCount = { ...state.productsCount, [id]: count };
				const totalCost = calculateTotalCost(products, productsCount)
				return { productsCount, products, totalCost };
			}
		}
		case 'remove':
			return state;
		case 'reset':
			return defaultBasket;
		default:
			return state;
	}
}
