import React from 'react';
import { RouterContext } from '../router';
import { routeToMenu } from '../router/actions';
import { BasketContext } from '../basket';
import { addProduct } from '../basket/actions';
import { Button } from '../components/button';
import { priceFormatter } from '../utils/price';

export function ProductPreview(product){
	const { imgSrc, description, cost, name, categoryId } = product;
	const routeDispatch = React.useContext(RouterContext);
	const basketDispatch = React.useContext(BasketContext);

	return (
			<div className="content_page bg-blur">
				<div className="header h-12">
				
			 	 </div>
			  <div className="content h-65">
			    <div className="row justify-content-md-center col-lg-12 h-100 p-0">
				  <div className="col-lg-4 h-100 transparent-color bg-blur custom-left-round">
					<h2 className="text-center  pt-5">{ name }</h2>
					<div className="row justify-content-md-center m-4 ">
						<div className="row col-lg-5">
						  <h4><span>&#9733;</span></h4>
						  <h4><span>&#9733;</span></h4>
						  <h4><span>&#9733;</span></h4>
						  <h4><span>&#9734;</span></h4>
						  <h4><span>&#9734;</span></h4>
						</div>
						<div className="col-log-3">
							<h4>
							{ priceFormatter(cost) } Руб
							</h4>
						</div>
				    </div>
					<div className="text-justify mt-5">
						{ description }
					</div>
					<Button onClick={ () => basketDispatch(addProduct(product)) }>Добавить в заказ</Button>
				  </div> 
				  <div className="col-lg-5 h-100 transparent-color">
				    <img className="h-100" src={imgSrc}/>
			      </div>
				  <div className="col-lg-1 h-100 transparent-color custom-right-round">
						<button
							onClick={ () => routeDispatch(routeToMenu(categoryId)) }
							className="btn m-1 text-size-100"
						>
							&times;
						</button>
			      </div>
			    </div>
			  </div>
			</div>
	);
}