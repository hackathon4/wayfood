import React from 'react';

import "./main-screen.css";
import { RouterContext } from '../router';
import { Button } from '../components/button'; 
import { routeToCategories } from '../router/actions';

export function MainScreen() {
	const routeDispatch = React.useContext(RouterContext);
	return (
		<>
			<div className="screen main-screen">
				<h1 className="main-screen__title">Way Food</h1>
				<Button 
					className="main-screen__button"
					onClick={() => routeDispatch(routeToCategories) }
				>
					Меню
				</Button>
			</div>
		</>
	);
}