import React from 'react';
import './button.css';

export function Button({ onClick, className = '', children }) {
	return (
		<button
			className={`${className} component__button`}
			onClick={ onClick }
		>
			{ children }
		</button>
	);
}