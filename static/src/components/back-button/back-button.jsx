import React from 'react';
import { returnBack } from '../../router/actions';
import { RouterContext } from '../../router';
import './back-button.css';

export function BackButton() {
	const routeDispatch = React.useContext(RouterContext);
	return (
		<button
			className="btn btn-ccircle btn-md m-3 back-button transparent-color"
			onClick={ () => routeDispatch(returnBack) }
		>
			<a className="text-size-20">&#8249;</a>
		</button>
	);
}