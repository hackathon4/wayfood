import React from 'react';
import { MenuCategories } from './menu-categories';
import { MenuPan } from './menu-pan';
import { MenuItem } from './menu-item';
import './menu-screen.css';

export function MenuScreen({categories, products = []}) {
	return (
		<div className="screen menu-screen">
			<MenuCategories categories={ categories }/>
			<MenuPan>
				{ products.map(product => <MenuItem key={`product_${product.id}`} { ...product }/>)}
			</MenuPan>
		</div>
	)
}