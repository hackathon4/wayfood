import React from 'react';
import './menu-pan.css';

export function MenuPan({children}) {
	return (
		<div className="menu-pan">{children}</div>
	)
}