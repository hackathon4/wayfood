import React from 'react';
import { Button } from '../../components/button';
import { RouterContext } from '../../router';
import { routeToMenu } from '../../router/actions';
import './category.css';

export function Category({ imgSrc, name, id }) {
	const routeDispatch = React.useContext(RouterContext);
	return (
		<div className="menu-category-container">
			<Button className="menu-category" onClick={ () => routeDispatch(routeToMenu(id)) }>
				<img src={ imgSrc } />{ name }
			</Button>
		</div>
	)
}