import React from 'react';
import { Category } from './category';
import "./menu-categories.css";

export function MenuCategories({ categories }) {
	return (
		<div className="menu-categories">
			{ categories.map( category => <Category key={`menu-category${category.id}`} {...category} /> ) }
		</div>
	)
}