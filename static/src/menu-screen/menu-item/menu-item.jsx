import React from 'react';
import './menu-item.css';
import { routeToProduct } from '../../router/actions';
import { RouterContext } from '../../router';

export function MenuItem(product) {
	const { imgSrc } = product;
	const routeDispatch = React.useContext(RouterContext);
	return (
		<img src={ imgSrc } className="menu-item" onClick={ () => routeDispatch(routeToProduct(product)) }/>
	)
}