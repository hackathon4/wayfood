import React from 'react';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/screen.css';

import { ProductPreview } from "./product-preview";
import { MainScreen } from './main-screen';
import { RouterContext, routerReducer, Route, initRouteState } from './router';
import { CategoriesScreen } from './categories-screen';
import { MenuScreen } from './menu-screen';
import { OrderScreen } from './order-screen';
import { BasketContext, routerBasket, Basket, defaultBasket } from './basket';
import { BackButton } from './components/back-button';

function productsToCategoryMap(products) {
  if (products === null) {
    return {};
  }

  const productsByCategory = {};
  for (let i = 0; i < products.length; i++) {
    const { categoryId } = products[i]
    if(categoryId in productsByCategory) {
      productsByCategory[categoryId].push(products[i])
    } else {
      productsByCategory[categoryId] = [products[i]];
    }
  }
  return productsByCategory;
}

function App() {
  const [{ route, basket, backButton, categoryId, product }, routeDispatch] = React.useReducer(routerReducer, initRouteState);
  const [basketProps, basketDispatch] = React.useReducer(routerBasket, defaultBasket)

  const [categories, setCategories] = React.useState(null);
  const [products, setProducts] = React.useState(null);

  React.useEffect(() => {
    fetch('http://45.129.3.142:3030/products').then((res) => res.json()).then(setProducts);
    fetch('http://45.129.3.142:3030/categories').then((res) => res.json()).then(setCategories);
  }, []);

  const productsByCategory = React.useMemo(() => productsToCategoryMap(products), [products])
  console.log(productsByCategory)
  console.log(basketProps);
  return (
      <RouterContext.Provider value={ routeDispatch }>
        <BasketContext.Provider value={ basketDispatch }>
          <div className="App__background">
          </div>
          <div className="App">

            { route === Route.Main && <MainScreen/> }
            { categories !== null && route === Route.Categories && <CategoriesScreen categories={ categories }/> }
            { categories !== null && route === Route.Menu && <MenuScreen categories={ categories } products={ productsByCategory[categoryId]} /> }
            { route === Route.Product && <ProductPreview {... product }/> }
            { route === Route.Order && <OrderScreen /> }
            { basket && <Basket { ...basketProps }/> }
            { backButton && <BackButton onClick={ () => {} } />}
          </div>
        </BasketContext.Provider>
      </RouterContext.Provider>
  );
}

export default App;
