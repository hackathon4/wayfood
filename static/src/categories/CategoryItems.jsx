import React from 'react';

import Item from './Item';

const CategoryItems = ({ category }) => {
    return (
        <>
            {category.map(item => <Item item={item}/>)}
        </>
    );
};

export default CategoryItems;