import React from 'react';

import CategoryItems from './CategoryItems';

const Categories = ({ categories = [] }) => {
    return (
        <div>
            {Object.keys(categories).map(category => <CategoryItems category={categories[category]} />)}
        </div>
    );
};

export default Categories;