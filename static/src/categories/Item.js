import React from 'react';

const Item = ({ item }) => {
	return ( // FIXME: refactor
		<div className="content_page bg-blur">
			<div className="header h-12">
			<button className="btn btn-dark btn-ccircle btn-md m-3"></button>
			</div>
			<div className="content h-75">
			<div className="row justify-content-md-center col-lg-12 h-100 p-0">
				<div className="col-lg-4 h-100 bg-light custom-left-round">
				<h2 className="text-center  pt-5">Красивый пример</h2>
				<div className="row m-2">
					<div className="row col-lg-8">
						<h4><span>&#9733;</span></h4>
						<h4><span>&#9733;</span></h4>
						<h4><span>&#9733;</span></h4>
						<h4><span>&#9734;</span></h4>
						<h4><span>&#9734;</span></h4>
					</div>
					<div className="col-log-3">
						<h4>
						180 Руб
						</h4>
					</div>
				</div>
				<div className="text-justify mt-5">
					То один из ключевых навыков официанта. Как в свое время академик Павлов, заставлял собачек истекать слюной, зажигая лампочку, так и официант должен уметь вызвать у гостя трепет предвкушения используя красивое описание блюд
				</div>
				<div className="row justify-content-md-center">
					<button className="btn btn-dark btn-ccircle btn-md">+</button>
					<div className="text-center"></div>
					<button className="btn btn-dark btn-ccircle btn-md">-</button>
				</div>
				</div> 
				<div className="col-lg-6 h-100 bg-light">
				<img className="h-100" alt={item.name} src={item.image}/>
				</div>
				<div className="col-lg-1 h-100 bg-light custom-right-round">
					<button className="btn btn-dark btn-ccircle btn-md m-3"></button>
				</div>
			</div>
			</div>
		<footer className="header h-12">
			<div></div>
		</footer>
		</div>
	);
}

export default Item;