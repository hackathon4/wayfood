module.exports = function(app, server){
  require('./static')(app);
  require('./api')(app);
};