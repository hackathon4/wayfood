const express = require('express');

module.exports = function(app){
  const router = express.Router();

  router.use(express.static(process.cwd() + '/static'));
  app.use(router);
}