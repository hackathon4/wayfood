const express = require('express');

module.exports = function(app){
  const router = express.Router();
  router.get('/products', require('./../api/products'));
  router.post('/add_product', require('./../api/add_product'));
  router.get('/categories', require('./../api/categories'));
  router.post('/payment', require('./../api/payment'));
 
  app.use(router);
}

