'use strict';
module.exports = (sequelize, DataTypes) => {
  const Category = sequelize.define('Category', {
    name: DataTypes.STRING,
    imgSrc: DataTypes.STRING
  }, {});
  Category.associate = function(models) {
    // associations can be defined here
    Category.hasMany(models.Product, {
        foreignKey: 'product_id',
        as: 'product'
    })

  };
  return Category;
};

/*
User.hasMany(models.Sshkey, {foreignKey: 'user_id'});
Staff.hasMany(models.Sshkey, {foreignKey: 'user_id'});

Sshkey.belongsTo(models.User, {foreignKey: 'user_id'});
Sshkey.belongsTo(models.Staff, {foreignKey: 'user_id'})
*/