'use strict';
module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    categoryId: DataTypes.INTEGER,
    cost: DataTypes.INTEGER,
    imgSrc: DataTypes.STRING
  }, {});
  Product.associate = function(models) {

    Produts.belongsTo(models.Category, {
      foreignKey: 'category_id',
      as: 'category'
    });
    /*
    Product.belongsTo(models.Category, {
      as: 'Category',
      foreignKey: 'categoryId'
    });
    */
    // associations can be defined here
  };  
  return Product;
};