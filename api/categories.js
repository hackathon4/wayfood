const { DataTypes } = require('sequelize');
const sequelize = require('../sequelize');


const Category = require('../models/category')(sequelize, DataTypes);

module.exports = function(req, res) {
  sequelize.sync().then(() => Category.findAll())
  .then(products => {
      res.send(products);
  }).catch(function(err) {
    // print the error details
    console.log(err);
  });
}