const { DataTypes } = require('sequelize');
const sequelize = require('../sequelize');
const { parse } = require('querystring');
const Product = require('../models/product')(sequelize, DataTypes);
const Category = require('../models/category')(sequelize, DataTypes);

module.exports = function(req, res) {
	console.log(req)
	if (req.method === 'POST') {
		let body = '';
		req.on('data', chunk => {
			body += chunk.toString(); // convert Buffer to string
		});
		req.on('end', () => {
			const parsedBody = parse(body);

			sequelize.sync()
				.then(() => Product.create(parsedBody)).then(() => {res.end('ok')}).catch(function(err) {
				console.log(err);
			})

			
		});
	}else {
		res('error')

	}
}