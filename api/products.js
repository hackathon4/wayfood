const { DataTypes } = require('sequelize');
const sequelize = require('../sequelize');

const Product = require('../models/product')(sequelize, DataTypes);
const Category = require('../models/category')(sequelize, DataTypes);

module.exports = function(req, res) {
  sequelize.sync().then(() => Product.findAll())
  .then(products => {
      res.send(products);
  }).catch(function(err) {
    // print the error details
    console.log(err);
  });
}

/*
sequelize.sync()
  .then(() => Category.create({
    name: "food",
    imgSrc: "food.jpg"
  }))
  .then(() => Category.findAll())
  .then(category => {
     console.log(category);
  }).then(() => Product.create({
    name: "pizza",
    description: "tastyPizzaa",
    cost: 10000,
    imgSrc: "pizza.jpg",
    categoryId: 1
  })).catch(function(err) {
    // print the error details
    console.log(err);
  })
*/
/*
sequelize.sync().then(() => Category.findAll()).then((cObj) => cObj.map((category) => Product.findAll({ where: { categoryId: category.id } }) ) ).then(product=> console.log(JSON.stringify(product))).catch((e) => console.log(e))
sequelize.sync().then(() => Product.findAll()).then((products) => products.map(product => Category.findByPk(product.categoryId))).then(product=> console.log(JSON.stringify(product))).catch((e) => console.log(e))
/*
sequelize.sync()
  .then(() => Category.create({
    name: "food",
    imgSrc: "food.jpg"
  }))
  .then(() => Category.findAll())
  .then(category => {
     console.log(category);
  }).catch(function(err) {
    // print the error details
    console.log(err);
  });
*/
/*
  .then(() => Product.create({
    name: "pizza",
    description: "tastyPizzaa",
    cost: 10000,
    img_src: "pizza.jpg",
  }))
  .then(() => Product.findAll())
  .then(jane => {
    
  });
*/