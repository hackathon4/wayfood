const express = require('express');
const http = require('http');

const log4js = require('log4js');
var cors = require('cors')

const logger = log4js.getLogger('wayfood');

const app = express();

app.use(cors())

const server = http.createServer(app);
const port = '3030'
logger.level = 'info';

require('./routers/index')(app, server);

server.listen(port, function(){
  logger.info(`nodejsstarter listening on http://localhost:${port}/appmetrics-dash`);
  logger.info(`nodejsstarter listening on http://localhost:${port}`);
});
